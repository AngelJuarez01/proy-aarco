import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable} from 'rxjs';
import { map, delay} from 'rxjs/operators';
import { descripcionModel } from '../models/descripcion.models';
import { marcaModel } from '../models/marca.models';
import { modeloModel } from '../models/modelo.models';
import { submarcaModel } from '../models/submarca.models';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  private url1= 'https://localhost:44376/api/marcas';
  private url2= 'https://localhost:44376/api/submarcas';
  private url3 = 'https://localhost:44376/api/modeloes';
  private url4 = 'https://localhost:44376/api/descripcions';
  private url5 = `https://api-test.aarco.com.mx/api-examen/api/examen/sepomex/{codigoPostal}`;
  private url6 = 'https://api-test.aarco.com.mx/api-examen/api/examen/crear-peticion';
  private url7 = 'https://api-test.aarco.com.mx/api-examen/api/examen/peticion/{peticionLlave}';

  getSubMarcasByMarca(marcaId: number): Observable<submarcaModel[]> {
    const url = `${this.url2}/GetSubmarcasByMarca/${marcaId}`;
    return this.http.get<submarcaModel[]>(url);
  }

  getModelosBySubMarca(marcaId: number, submarcaId: number): Observable<modeloModel[]> {
    const url = `${this.url3}/GetModelosBySubmarca/${marcaId}/${submarcaId}`;
    return this.http.get<modeloModel[]>(url);
  }

  getDescripcionByModelo(modeloId: number): Observable<descripcionModel[]> {
    const url = `${this.url4}/GetDescripcionByModelo/${modeloId}`;
    return this.http.get<descripcionModel[]>(url);
  }


  constructor(private http:HttpClient) { }

  getApi1Data(): Observable<any>{
    return this.http.get<any>(this.url1);
  }

  getApi2Data(): Observable<any>{
    return this.http.get<any>(this.url2)
  }

  getApi3Data(): Observable<any>{
    return this.http.get<any>(this.url3)
  }

  getApi4Data(): Observable<any>{
    return this.http.get<any>(this.url4)
  }

  getCombineData(): Observable<any>{
    return forkJoin([this.getApi1Data(), this.getApi2Data(), this.getApi3Data(), this.getApi4Data()]).pipe(map(([url1data, url2data, url3data, url4data]) => {
      return { url1data, url2data, url3data, url4data};
    }))
  }
}
