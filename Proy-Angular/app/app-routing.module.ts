import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarComponent } from './page/listar/listar.component';

const routes: Routes = [
  {path:'listar', component:ListarComponent },
  {path:'**', pathMatch:'full',redirectTo:'listar'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
