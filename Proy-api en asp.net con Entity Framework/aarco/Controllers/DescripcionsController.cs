﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using aarco.Models;

namespace aarco.Controllers
{
    public class DescripcionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/Descripcions
        public IQueryable<Descripcion> GetDescripcion()
        {
            return db.Descripcion;
        }

        // GET: api/Descripcions/5
        [ResponseType(typeof(Descripcion))]
        public IHttpActionResult GetDescripcion(int id)
        {
            Descripcion descripcion = db.Descripcion.Find(id);
            if (descripcion == null)
            {
                return NotFound();
            }

            return Ok(descripcion);
        }

        // PUT: api/Descripcions/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutDescripcion(int id, Descripcion descripcion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != descripcion.Id)
            {
                return BadRequest();
            }

            db.Entry(descripcion).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DescripcionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Descripcions
        [ResponseType(typeof(Descripcion))]
        public IHttpActionResult PostDescripcion(Descripcion descripcion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Descripcion.Add(descripcion);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = descripcion.Id }, descripcion);
        }

        // DELETE: api/Descripcions/5
        [ResponseType(typeof(Descripcion))]
        public IHttpActionResult DeleteDescripcion(int id)
        {
            Descripcion descripcion = db.Descripcion.Find(id);
            if (descripcion == null)
            {
                return NotFound();
            }

            db.Descripcion.Remove(descripcion);
            db.SaveChanges();

            return Ok(descripcion);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DescripcionExists(int id)
        {
            return db.Descripcion.Count(e => e.Id == id) > 0;
        }
    }
}