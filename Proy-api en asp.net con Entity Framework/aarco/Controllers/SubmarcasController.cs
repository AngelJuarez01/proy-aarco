﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.UI.WebControls;
using aarco.Models;
using System.Web.Http.Cors;

namespace aarco.Controllers
{
    public class SubmarcasController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();


        // GET: api/Submarcas
        public IQueryable<Submarca> GetSubmarca()
        {
            return db.Submarca;
        }

        // GET: api/Submarcas/5
        [ResponseType(typeof(Submarca))]
        public IHttpActionResult GetSubmarca(int id)
        {
            Submarca submarca = db.Submarca.Find(id);
            if (submarca == null)
            {
                return NotFound();
            }

            return Ok(submarca);
        }

        // PUT: api/Submarcas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSubmarca(int id, Submarca submarca)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != submarca.Id)
            {
                return BadRequest();
            }

            db.Entry(submarca).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubmarcaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Submarcas
        [ResponseType(typeof(Submarca))]
        public IHttpActionResult PostSubmarca(Submarca submarca)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Submarca.Add(submarca);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = submarca.Id }, submarca);
        }

        // DELETE: api/Submarcas/5
        [ResponseType(typeof(Submarca))]
        public IHttpActionResult DeleteSubmarca(int id)
        {
            Submarca submarca = db.Submarca.Find(id);
            if (submarca == null)
            {
                return NotFound();
            }

            db.Submarca.Remove(submarca);
            db.SaveChanges();

            return Ok(submarca);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SubmarcaExists(int id)
        {
            return db.Submarca.Count(e => e.Id == id) > 0;
        }
    }
}