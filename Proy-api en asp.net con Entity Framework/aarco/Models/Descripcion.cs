namespace aarco.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Descripcion")]
    public partial class Descripcion
    {
        [Column("Descripcion")]
        [Required]
        [StringLength(100)]
        public string Descripcion1 { get; set; }

        [Required]
        [StringLength(50)]
        public string DescripcionId { get; set; }

        public int Id { get; set; }
    }
}
