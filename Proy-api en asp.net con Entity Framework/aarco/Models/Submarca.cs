namespace aarco.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Submarca")]
    public partial class Submarca
    {
        [Column("Submarca")]
        [Required]
        [StringLength(50)]
        public string Submarca1 { get; set; }

        public int Id { get; set; }
    }
}
