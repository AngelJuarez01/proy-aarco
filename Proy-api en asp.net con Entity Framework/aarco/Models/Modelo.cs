namespace aarco.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Modelo")]
    public partial class Modelo
    {
        [Column("Modelo")]
        public short Modelo1 { get; set; }

        public int Id { get; set; }
    }
}
