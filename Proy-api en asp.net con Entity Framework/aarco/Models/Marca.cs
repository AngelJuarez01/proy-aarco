namespace aarco.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Marca")]
    public partial class Marca
    {
        [Column("Marca")]
        [Required]
        [StringLength(50)]
        public string Marca1 { get; set; }

        public int Id { get; set; }
    }
}
